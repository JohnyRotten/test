'use strict';

var gulp			= require('gulp'),
	prefixer		= require('gulp-autoprefixer'),
	connect			= require('gulp-connect'),
	cssmin			= require('gulp-cssmin'),
	imagemin		= require('gulp-imagemin'),
	pngquant		= require('imagemin-pngquant'),
	imageResize		= require('gulp-image-resize'),
	less			= require('gulp-less'),
	sourcemaps		= require('gulp-sourcemaps'),
	rigger			= require('gulp-rigger'),
	uglify			= require('gulp-uglify'),
	watch			= require('gulp-watch'),
	opn				= require('opn'),
	rimraf			= require('rimraf');

var path = {
	build: {
		html: 'build/',
		js: 'build/js/',
		css: 'build/css/',
		img: 'build/images/',
		fonts: 'build/fonts/'
	},
	src: {
		html: 'src/*.html',
		js: 'src/js/main.js',
		css: 'src/css/main.less',
		img: 'src/images/**/*.*',
		fonts: 'src/fonts/**/*.*'
	},
	watch: {
		html: 'src/**/*.html',
		js: 'src/js/**/*.js',
		css: 'src/css/**/*.less',
		img: 'src/images/**/*.*',
		fonts: 'src/fonts/**/*.*'
	},
	clean: './build'
};

var server = {
	host: 'localhost',
	port: '9000'
};

gulp.task('html:build', function(){
	gulp.src(path.src.html)
		.pipe(rigger())
		.pipe(gulp.dest(path.build.html))
		.pipe(connect.reload());
});

gulp.task('js:build', function(){
	gulp.src(path.src.js)
		.pipe(rigger())
		.pipe(sourcemaps.init())
		.pipe(uglify())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(path.build.js))
		.pipe(connect.reload());
});

gulp.task('css:build', function(){
	gulp.src(path.src.css)
		.pipe(rigger())
		.pipe(sourcemaps.init())
		.pipe(less())
		.pipe(prefixer())
		.pipe(cssmin())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(path.build.css))
		.pipe(connect.reload());
});

gulp.task('image:build', function(){
	gulp.src(path.src.img)
		// .pipe(imageResize({
		// 	height: 800	
		// }))
		.pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()],
			interlaced: true
		}))
		.pipe(gulp.dest(path.build.img))
		.pipe(connect.reload());
});

gulp.task('fonts:build', function() {
	gulp.src(path.src.fonts)
		.pipe(gulp.dest(path.build.fonts))
		.pipe(connect.reload());
});

gulp.task('build', [
	'html:build',
	'js:build',
	'css:build',
	'image:build',
	'fonts:build'
]);

gulp.task('watch', function(){
	watch([path.watch.html], function(){
		try {
			gulp.start('html:build');
		} catch (e) {
			console.log(e)
		}
	});
	watch([path.watch.js], function(){
		gulp.start('js:build');
	});
	watch([path.watch.css], function(){
		gulp.start('css:build');
	});
	watch([path.watch.fonts], function(){
		gulp.start('fonts:build');
	});
	watch([path.watch.img], function(){
		gulp.start('image:build');
	});
});

gulp.task('webserver', function(){
	connect.server({
		host: server.host,
		port: server.port,
		livereload: true
	});
});

gulp.task('clean', function(cb){
	rimraf(path.clean, cb);
});

gulp.task('openbrowser', function(){
	opn('http://' + server.host + ':' + server.port + '/build');
});

gulp.task('default', ['build', 'webserver', 'watch', 'openbrowser']);