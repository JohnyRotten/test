$ = $ || {};
$.dialog = function(message, h1, h2){
	var body = $('<div/>').addClass('inner');
	if (!!h1) body.append($('<h1/>', { text: h1 }));
	if (!!h2) body.append($('<h2/>', { text: h2 }));
	body.append(message);
	var close = $('<i>').addClass('close')
						.addClass('mdi')
						.addClass('mdi-close');
	close.click(function(){
		$('#dialog').remove();
		return false;
	});
	close.appendTo(body);
	$('body').append($('<div/>', { id: 'dialog' }).html(body));
	console.log(1);
};

$('#open').click(function(){
	$.dialog('Test', 'Hello, World!', 'Good new, everyone.');
	return false;
});