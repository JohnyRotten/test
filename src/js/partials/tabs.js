$ = $ || {};

$.tabs = function(e) {
	var tabs = $(e);
	var nav = tabs.find('.tabs-nav');
	var content = tabs.find('.tabs-content');
	nav.find('li').first().addClass('active');
	content.find('li').first().addClass('active');
	nav.find('li').click(function(){
		var curr = $(this);
		nav.find('li').removeClass('active');
		curr.addClass('active');
		content.find('li').removeClass('active');
		target = curr.data('target');
		console.log(target);
		cont = content.find('[data-target=' + target + ']');
		cont.addClass('active');
	});
};

$.tabs('.tabs');