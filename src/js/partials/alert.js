function log(message, type) {
	type = type || 'message';
	var d = $('<div/>').addClass('alert')
						.addClass('alert-' + type)
						.text(message);
	var close = $('<i/>').addClass('close')
						 .addClass('mdi')
						 .addClass('mdi-close');
	close.click(function(){
		d.remove();
		return false;
	});
	close.appendTo(d);
	var box = $('#alert_box');
	if (box.length) {
		box.append(d);
	} else {
		$('<div/>', { id: 'alert_box' }).append(d)
			.appendTo('#main');
	}
}