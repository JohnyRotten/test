$ = $ || {};
$.gallery = function(e){
	$(e).find('li').click(function(){
		var close = $('<i>').addClass('close')
							.addClass('mdi')
							.addClass('mdi-close');
		var prev = $('<i>').addClass('prev')
							.addClass('mdi')
							.addClass('mdi-chevron-left');
		var next = $('<i>').addClass('next')
							.addClass('mdi')
							.addClass('mdi-chevron-right');
		var p = $(this);
		$(e).find('.opened').removeClass('opened');
		$(e).find('.close').remove();
		$(e).find('.prev').remove();
		$(e).find('.next').remove();
		p.addClass('opened');
		close.click(function(){
			p.removeClass('opened');
			$(e).find('.prev').remove();
			$(e).find('.next').remove();
			$(this).remove();
			return false;
		});
		prev.click(function(){
			var _p = prev.parent().prev();
			if (_p.length) {
				$(_p).trigger('click');
			} else {
				$(prev.last().trigger('click'));
			}
			return false;
		});
		next.click(function(){
			var _n = next.parent().next();
			if (_n.length) {
				$(_n).trigger('click');
			} else {
				$(next.first().trigger('click'));
			}
			return false;
		});
		prev.appendTo($(this));
		next.appendTo($(this));
		close.appendTo($(this));
		var top = p.offset().top + -50;
		$('html, body').animate({scrollTop: top}, 300);
		return false;
	});
};

$.gallery('#gallery');