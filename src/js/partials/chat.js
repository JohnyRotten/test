// Chat
$ = $ || {};

$.chat = function(e) {
	var ws;
	var i = $(e);
	if (WebSocket === undefined) {
		i.text('WebSocket not work');
		return;
	}
	ws = new WebSocket("ws://localhost/ws");
	ws.onopen = function(event){
		console.log('WS opened');
	};
	ws.onmessage = function(event){
		console.log('Message: ' + event.data);
	};
	ws.onclose = function(event){
		console.log('WS closed');
		i.text('Connection was closed')
	};
	ws.onerror = function(event){
		console.log('WS error: ' + e.message);
	};
};

$.chat('#chat');